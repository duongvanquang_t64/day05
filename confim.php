<?php
$gioitinh = array(0 => "Nam", 1 => "Nữ");

$phankhoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$fileDestination = '';
define("UPLOAD_FOLDER", 'uploads/');
if (isset($_FILES['file'])) {
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $filType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileSize = $_FILES['file']['size'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(array_pop($fileExt));

    if ($fileError === 0) {
        // $fileNameNew = uniqid('', true);
        $fileNameNew = date('YmdHis');
        $fileDestination = UPLOAD_FOLDER . str_replace(' ', '', join('.', $fileExt)) . "_" . $fileNameNew . "." . $fileActualExt;
        if (!file_exists(UPLOAD_FOLDER)) {
            mkdir(UPLOAD_FOLDER, 0777, true);
        }
        if (!move_uploaded_file($fileTmpName, $fileDestination)) {
            // Moves an uploaded file to a new location error
        }
    } else {
        // image upload error
    }
}
$data = array(
    'imageUrl' => $fileDestination
);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        .room {
            width: 700px;
            height: 600px;
            display: block;
            border: 1px solid #007fff;
            margin: auto;
            margin-top: 50px;


        }

        .items {
            display: flex;
            align-items: center;
            margin-left: 60px;
        }



        .content {
            width: 90px;
            height: 20px;
            padding: 10px 0px;
            text-align: center;
            display: block;
            background-color: #00bf00;
            border: 1px solid #007fff;
        }

        .iptext {
            width: 300px;
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
        }

        .ipdate {
            height: 35px;
            border: 1px solid #007fff;
            width: 200px;
            margin-left: 20px;
        }

        .phankhoa {
            height: 35px;
            border: 1px solid #007fff;
            margin-left: 20px;
            width: 200px;
        }

        .ipgioitinh {
            margin-left: 20px;
        }

        .btn {
            padding: 15px;
            background-color: #00bf00;
            border: 1px solid #007fff;
            border-radius: 10px;
            margin-left: 250px;

        }

        .post {

            margin-left: 20px;
            font-size: 18px;
        }
    </style>
</head>

<body>
    <form action="confim.php" method="post">

        <div class="room">
            <div class="items" style="margin-top: 40px;">
                <p class="content">Họ và Tên</p>
                <p class="post">
                    <?php
                    $u = $_POST['name'];
                    echo $u;
                    ?>
                </p>


            </div>
            <div class="items">
                <p class="content" style="margin-right: 20px;">Giới tính</p>

                <p class="post">
                    <?php
                    $u = $_POST['gioitinh'];
                    echo $gioitinh[$u];
                    ?>
                </p>


            </div>
            <div class="items">
                <p class="content">
                    <label for="phankhoa">
                        Phân khoa
                    </label>
                </p>
                <p class="post">
                    <?php
                    $u = $_POST['phankhoa'];
                    echo $phankhoa[$u];
                    ?>
                </p>


            </div>
            <div class="items">
                <p class="content">Ngày sinh</p>
                <p class="post">
                    <?php
                    $u = $_POST['ngaysinh'];
                    echo $u;
                    ?>
                </p>


            </div>
            <div class="items">
                <p class="content">Địa chỉ</p>
                <p class="post">
                    <?php
                    $u = $_POST['diachi'];
                    echo $u;
                    ?>
                </p>

            </div>
            <div class="items">
                <p class="content">
                    <label for="file">
                        Hình ảnh
                    </label>
                </p>
                <p style="margin-left: 20px;">
                    <?php
                    $link  = htmlspecialchars($data['imageUrl']);
                    echo !$data['imageUrl'] ? "<font>Không có ảnh</font>" : "<img src=$link alt='image'
                    style='object-fit:cover;
                    width:160px;
                    height:80px;
                    border: solid 1px #CCC'; >" ?>
                </p>
            </div>
            <div>
                <input type="submit" value="Xác Nhận" class="btn" name="submit">
            </div>

        </div>
    </form>
</body>

</html>